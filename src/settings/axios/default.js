import axios from "axios";

const defaultInstance = axios.create({
  baseURL: "https://5ce8fa84a8c1ee0014c7034f.mockapi.io/"
});

export default defaultInstance;
