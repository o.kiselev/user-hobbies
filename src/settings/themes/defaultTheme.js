import { css } from "styled-components";

const sizes = {
  tablet: 1400,
  phone: 576
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)}
    }
  `;
  return acc;
}, {});

export const theme = {
  palette: {
    common: {
      white: "#fff",
      black: "#000",
      grey: "#615f5f"
    }
  }
};
