import React, { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import * as C from "./Layout.style";
import Header from "../../components/Header/Header";
import Users from "../../components/Users/Users";
import Hobbies from "../../components/Hobbies/Hobbies";
import usersActions from "../../redux/users/actions";
import hobbiesActions from "../../redux/hobbies/actions";

const Layout = ({
  getUsers,
  addUser,
  selectUser,
  getUserHobbies,
  addUserHobby,
  deleteUserHobby,
  users,
  hobbies
}) => {
  // Hooks
  // Get users on initial mount
  useEffect(() => {
    getUsers();
  }, []);

  return (
    <C.Wrapper>
      <Header />

      <C.Content>
        <Users users={users} addUser={addUser} selectUser={selectUser} />
        <Hobbies
          hobbies={hobbies}
          selectedUser={users.selectedUser}
          getUserHobbies={getUserHobbies}
          addUserHobby={addUserHobby}
          deleteUserHobby={deleteUserHobby}
        />
      </C.Content>
    </C.Wrapper>
  );
};

// Redux
const mapStateToProps = state => ({
  users: state.users,
  hobbies: state.hobbies.hobbies
});

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(usersActions.getUsersRequest()),
  addUser: user => dispatch(usersActions.addUserRequest(user)),
  selectUser: userId => dispatch(usersActions.selectUser(userId)),
  getUserHobbies: userId =>
    dispatch(hobbiesActions.getUserHobbiesRequest(userId)),
  addUserHobby: (userId, hobby) =>
    dispatch(hobbiesActions.addUserHobbyRequest(userId, hobby)),
  deleteUserHobby: (userId, hobbyId) =>
    dispatch(hobbiesActions.deleteUserHobbyRequest(userId, hobbyId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);

// Prop-types
Layout.propTypes = {
  getUsers: PropTypes.func,
  addUser: PropTypes.func,
  selectUser: PropTypes.func,
  getUserHobbies: PropTypes.func,
  addUserHobby: PropTypes.func,
  deleteUserHobby: PropTypes.func,
  users: PropTypes.array,
  hobbies: PropTypes.array
};
