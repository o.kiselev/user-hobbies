/* eslint-disable func-names */
import { all, takeEvery, put, fork } from "redux-saga/effects";
import axios from "../../settings/axios/default";

import actions from "./actions";

export function* getUsers() {
  yield takeEvery(actions.GET_USERS_REQUEST, function*() {
    try {
      const response = yield axios.get("/users");
      yield put(actions.getUsers(response.data));
    } catch (e) {
      console.log(e);
    }
  });
}

export function* addUser() {
  yield takeEvery(actions.ADD_USER_REQUEST, function*(action) {
    try {
      const response = yield axios.post("/users", action.payload.user);
      yield put(actions.addUser(response.data));
    } catch (e) {
      console.log(e);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getUsers), fork(addUser)]);
}
