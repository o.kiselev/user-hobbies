const actions = {
  GET_USERS_REQUEST: "GET_USERS_REQUEST",
  GET_USERS: "GET_USERS",
  ADD_USER_REQUEST: "ADD_USER_REQUEST",
  ADD_USER: "ADD_USER",
  SELECT_USER: "SELECT_USER",
  getUsersRequest: () => ({
    type: actions.GET_USERS_REQUEST
  }),
  getUsers: users => ({
    type: actions.GET_USERS,
    payload: {
      users
    }
  }),
  addUserRequest: user => ({
    type: actions.ADD_USER_REQUEST,
    payload: {
      user
    }
  }),
  addUser: user => ({
    type: actions.ADD_USER,
    payload: {
      user
    }
  }),
  selectUser: userId => ({
    type: actions.SELECT_USER,
    payload: {
      userId
    }
  })
};

export default actions;
