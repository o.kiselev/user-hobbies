import actions from "./actions";

const initialState = {
  allUsers: [],
  selectedUser: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_USERS:
      return {
        ...state,
        allUsers: action.payload.users
      };
    case actions.ADD_USER:
      return {
        ...state,
        allUsers: [action.payload.user, ...state.allUsers]
      };
    case actions.SELECT_USER:
      return {
        ...state,
        selectedUser: {
          ...state.allUsers.find(user => user.id === action.payload.userId)
        }
      };
    default:
      return state;
  }
};

export default reducer;
