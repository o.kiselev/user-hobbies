/* eslint-disable func-names */
import { all, takeEvery, put, fork } from "redux-saga/effects";
import axios from "../../settings/axios/default";

import actions from "./actions";

export function* getUserHobbies() {
  yield takeEvery(actions.GET_USER_HOBBIES_REQUEST, function*(action) {
    try {
      const response = yield axios.get(
        `/users/${action.payload.userId}/hobbies`
      );
      yield put(actions.getUserHobbies(response.data));
    } catch (e) {
      console.log(e);
    }
  });
}

export function* addUserHobby() {
  yield takeEvery(actions.ADD_USER_HOBBY_REQUEST, function*(action) {
    try {
      const response = yield axios.post(
        `/users/${action.payload.userId}/hobbies`,
        action.payload.hobby
      );
      yield put(actions.addUserHobby(response.data));
    } catch (e) {
      console.log(e);
    }
  });
}

export function* deleteUserHobby() {
  yield takeEvery(actions.DELETE_USER_HOBBY_REQUEST, function*(action) {
    try {
      yield axios.delete(
        `/users/${action.payload.userId}/hobbies/${action.payload.hobbyId}`,
        action.payload.hobbyId
      );
      yield put(actions.deleteUserHobby(action.payload.hobbyId));
    } catch (e) {
      console.log(e);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getUserHobbies), fork(addUserHobby), fork(deleteUserHobby)]);
}
