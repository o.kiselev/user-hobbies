import actions from "./actions";

const initialState = {
  hobbies: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_USER_HOBBIES:
      return {
        ...state,
        hobbies: action.payload.hobbies
      };
    case actions.ADD_USER_HOBBY:
      return {
        ...state,
        hobbies: [action.payload.hobby, ...state.hobbies]
      };
    case actions.DELETE_USER_HOBBY:
      return {
        ...state,
        hobbies: state.hobbies.filter(
          hobby => hobby.id !== action.payload.hobbyId
        )
      };
    default:
      return state;
  }
};

export default reducer;
