const actions = {
  GET_USER_HOBBIES_REQUEST: "GET_USER_HOBBIES_REQUEST",
  GET_USER_HOBBIES: "GET_USER_HOBBIES",
  ADD_USER_HOBBY_REQUEST: "ADD_USER_HOBBY_REQUEST",
  ADD_USER_HOBBY: "ADD_USER_HOBBY",
  DELETE_USER_HOBBY_REQUEST: "DELETE_USER_HOBBY_REQUEST",
  DELETE_USER_HOBBY: "DELETE_USER_HOBBY",
  getUserHobbiesRequest: userId => ({
    type: actions.GET_USER_HOBBIES_REQUEST,
    payload: {
      userId
    }
  }),
  getUserHobbies: hobbies => ({
    type: actions.GET_USER_HOBBIES,
    payload: {
      hobbies
    }
  }),
  addUserHobbyRequest: (userId, hobby) => ({
    type: actions.ADD_USER_HOBBY_REQUEST,
    payload: {
      userId,
      hobby
    }
  }),
  addUserHobby: hobby => ({
    type: actions.ADD_USER_HOBBY,
    payload: {
      hobby
    }
  }),
  deleteUserHobbyRequest: (userId, hobbyId) => ({
    type: actions.DELETE_USER_HOBBY_REQUEST,
    payload: {
      userId,
      hobbyId
    }
  }),
  deleteUserHobby: hobbyId => ({
    type: actions.DELETE_USER_HOBBY,
    payload: {
      hobbyId
    }
  })
};

export default actions;
