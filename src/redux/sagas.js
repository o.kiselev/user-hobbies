import { all } from "redux-saga/effects";

import users from "./users/saga";
import hobbies from "./hobbies/saga";

export default function* rootSaga() {
  yield all([users(), hobbies()]);
}
