import React from "react";
import { ThemeProvider } from "styled-components";

import Layout from "./containers/Layout/Layout";
import { theme } from "./settings/themes/defaultTheme";

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Layout />
    </ThemeProvider>
  );
};

export default App;
