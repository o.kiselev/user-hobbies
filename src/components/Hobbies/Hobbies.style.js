import styled from "styled-components";

export const Wrapper = styled.div`
  width: 80%;
  height: 100%;
  border: 1px solid ${props => props.theme.palette.common.black};
  border-left: none;
  display: flex;
  flex-direction: column;
`;

export const InputsWrapper = styled.div`
  height: 3rem;
  width: 100%;
  display: flex;

  .Dropdown-root {
    width: 30%;
  }

  .Dropdown-control {
    height: 100%;
    border: 1px solid ${props => props.theme.palette.common.black};
    border-left: none;
    border-top: none;
    display: flex;
    align-items: center;
  }

  .Dropdown-arrow {
    top: 20px;
  }
`;

export const Input = styled.input`
  height: 100%;
  width: 30%;
  border: 1px solid ${props => props.theme.palette.common.black};
  border-left: none;
  border-top: none;
  padding: 0 0 0 0.5rem;
`;

export const Button = styled.button`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  border-left: none;
  cursor: pointer;
  background: transparent;
`;

export const DeleteButton = styled.button`
  display: flex;
  height: 1rem;
  width: 1rem;
  justify-content: center;
  align-items: center;
  background: transparent;
  border: 5px solid darkred;
  margin: 0 0 0 0.5rem;
  cursor: pointer;
`;

export const DeleteWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`;

export const List = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  flex: 1;
  overflow: auto;
`;

export const ListItem = styled.div`
  display: flex;
  width: 100%;
  min-height: 3rem;
  align-items: center;
  justify-content: space-between;
  padding: 0 1rem;
  border: 1px solid ${props => props.theme.palette.common.black};
  border-top: none;
  border-left: none;

  > span:first-of-type {
    width: 150px;
  }
`;
