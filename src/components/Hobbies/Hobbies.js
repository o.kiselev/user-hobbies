import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import moment from "moment";

import * as C from "./Hobbies.style";

const Hobbies = ({
  hobbies,
  selectedUser,
  getUserHobbies,
  addUserHobby,
  deleteUserHobby
}) => {
  // Variables
  const initialValues = {
    passionLevel: "",
    name: "",
    createdAt: ""
  };

  // Hooks
  // Get hobbies of selected user
  useEffect(() => {
    if (selectedUser.id) {
      getUserHobbies(selectedUser.id);
    }
  }, [selectedUser]);

  // Form state
  const [values, setValues] = useState(initialValues);

  // Handlers
  const onSubmitHandler = () => {
    if (Object.values(values).every(Boolean)) {
      addUserHobby(selectedUser.id, {
        ...values,
        createdAt: moment(values.createdAt, "YYYY").format()
      });
      setValues(initialValues);
    }
  };

  return selectedUser.id ? (
    <C.Wrapper>
      <C.InputsWrapper>
        <Dropdown
          options={["Low", "Medium", "High", "Very-High"]}
          placeholder="Select passion level"
          value={values.passionLevel}
          onChange={event =>
            setValues({ ...values, passionLevel: event.value })
          }
        />
        <C.Input
          type="text"
          placeholder="Enter user hobby"
          value={values.name}
          onChange={event => setValues({ ...values, name: event.target.value })}
        />
        <C.Input
          type="number"
          placeholder="Enter year"
          value={values.createdAt}
          onChange={event =>
            setValues({
              ...values,
              createdAt: event.target.value
            })
          }
        />
        <C.Button type="button" onClick={() => onSubmitHandler()}>
          ADD
        </C.Button>
      </C.InputsWrapper>

      <C.List>
        {hobbies.length
          ? hobbies.map(hobby => (
              <C.ListItem key={hobby.id}>
                <span>Passion: {hobby.passionLevel}</span>
                <span>{hobby.name}</span>
                <C.DeleteWrapper>
                  <span>Since {moment(hobby.createdAt).format("YYYY")}</span>
                  <C.DeleteButton
                    onClick={() => deleteUserHobby(selectedUser.id, hobby.id)}
                  />
                </C.DeleteWrapper>
              </C.ListItem>
            ))
          : null}
      </C.List>
    </C.Wrapper>
  ) : (
    <C.Wrapper style={{ background: "#9a9a9a" }} />
  );
};

export default Hobbies;

Hobbies.propTypes = {
  hobbies: PropTypes.array,
  selectedUser: PropTypes.object,
  getUserHobbies: PropTypes.func,
  addUserHobby: PropTypes.func,
  deleteUserHobby: PropTypes.func
};
