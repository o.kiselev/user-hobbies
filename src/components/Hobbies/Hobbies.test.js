import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Hobbies from "./Hobbies";
import * as C from "./Hobbies.style";

configure({ adapter: new Adapter() });

describe("<Hobbies />", () => {
  let wrapper;
  let testProps = {
    selectedUser: {},
    hobbies: []
  };

  beforeEach(() => {
    wrapper = shallow(<Hobbies {...testProps} />);
  });

  it("should render", () => {
    expect(wrapper.exists()).toBe(true);
  });

  it("should render list of hobbies if user ID is true", () => {
    wrapper.setProps({ selectedUser: { id: "some-id", name: "some-name" } });
    expect(wrapper.find(C.List).exists()).toBe(true);
  });
});
