import React, { useState } from "react";
import PropTypes from "prop-types";

import * as C from "./Users.style";

const Users = ({ users: { allUsers, selectedUser }, addUser, selectUser }) => {
  // Hooks
  // Input state
  const [nameValue, setNameValue] = useState("");

  // Handle user creation
  const addUserHandler = () => {
    // Check if we have only letters
    if (!/[^a-z]/i.test(nameValue) && nameValue) {
      addUser({ name: nameValue });
      setNameValue("");
    } else {
      alert("Must contain letters!");
      setNameValue("");
    }
  };

  return (
    <C.Wrapper>
      <C.InputWrapper>
        <C.Input
          value={nameValue}
          type="text"
          placeholder="Enter user name"
          onChange={event => setNameValue(event.target.value)}
        />
        <C.Button type="button" onClick={() => addUserHandler()}>
          ADD
        </C.Button>
      </C.InputWrapper>

      <C.List>
        {allUsers.map(user => (
          <C.ListItem
            key={user.id}
            className={user.id === selectedUser.id ? "selected" : null}
            onClick={() => selectUser(user.id)}
          >
            {user.name}
          </C.ListItem>
        ))}
      </C.List>
    </C.Wrapper>
  );
};

export default Users;

// Prop-types
Users.propTypes = {
  users: PropTypes.object,
  allUsers: PropTypes.array,
  selectedUser: PropTypes.object,
  addUser: PropTypes.func,
  selectUser: PropTypes.func
};
