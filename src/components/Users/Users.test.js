import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Users from "./Users";
import * as C from "./Users.style";

configure({ adapter: new Adapter() });

describe("<Users />", () => {
  let wrapper;
  let testProps;

  beforeEach(() => {
    testProps = {
      users: {
        allUsers: [],
        selectedUser: {}
      }
    };
    wrapper = shallow(<Users {...testProps} />);
  });

  it("should render", () => {
    expect(wrapper.exists()).toBe(true);
  });

  it("should contain list of users", () => {
    expect(wrapper.find(C.List).exists()).toBe(true);
    expect(wrapper.find(C.List).children()).toHaveLength(
      testProps.users.allUsers.length
    );
  });
});
