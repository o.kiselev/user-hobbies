import styled from "styled-components";

export const Wrapper = styled.div`
  width: 20%;
  min-width: 100px;
  height: 100%;
  border: 1px solid ${props => props.theme.palette.common.black};
  display: flex;
  flex-direction: column;
`;

export const InputWrapper = styled.div`
  height: 3rem;
  width: 100%;
  display: flex;
`;

export const Input = styled.input`
  height: 100%;
  width: 75%;
  border: 1px solid ${props => props.theme.palette.common.black};
  border-top: none;
  padding: 0 0 0 0.5rem;
`;

export const Button = styled.button`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  border-left: none;
  border-right: none;
  cursor: pointer;
  background: transparent;
`;

export const List = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  flex: 1;
  overflow: auto;
`;

export const ListItem = styled.div`
  display: flex;
  width: 100%;
  min-height: 3rem;
  align-items: center;
  justify-content: center;
  border: 1px solid ${props => props.theme.palette.common.black};
  border-top: none;
  border-right: none;
  cursor: pointer;

  :hover,
  &.selected {
    font-weight: bold;
  }
`;
