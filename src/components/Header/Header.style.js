import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 0 2rem;
  height: 5rem;
  font-size: 1.5rem;
`;

export default Wrapper;
